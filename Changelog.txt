Práctica 1 51751
Se ha creado el archivo de texto Changelog usando "sudo nano Changelog.txt"

Se ha modificado el nombre del directorio "practica4" a "practica5" en el directorio sii-51751 usando "mv practica4 practica5"

Se ha actualizado el repositorio confirmando los cambios indicados por "git status" y "git add Changelog.txt" (por ejemplo)

Se ha creado una rama de prueba "git branch rama_prueba"

Se ha realizado el commit "git commit -m 'mensaje del commit' "

Se ha actualizado el repositorio remoto "git push origin master"

Se ha creado una etiqueta para el repositorio "git tag -a v1.1 -m 'Curso 2018/19 Mensaje version 1.1' "

Se ha eliminado el directorio practica4 "rm -r practica4"

Nota después de eliminar un fichero o directorio si realizamos un commit los cambios se guardarán en cambio si añadimos elementos hay que hacer un "git add <cosaCambiada>" para que los cambios se mantengan

Se ha modificado el código para que después de cada rebote la esfera cambie de color y el radio aumente o disminuya de radio con cada colisión con las raquetas.